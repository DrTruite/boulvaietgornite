# Comment utiliser git ?

### Avant de commencer ma session de travail

> git pull
	
Cette commande permet d'etre sur d'avoir le dossier bien a jours.

### J'ai fini un truc et je veut "l'enregistrer"

> git add .
> 
> git commit -m "Mon message"

Ces deux commandes a la suite permettent d'enregistrer les changements sur git.

### J'ai fini de faire des trucs pour le moment et je veut envoyer mes changements a Victor

> git push

Cette commande permet d'envoyer tous mes changement sur le repository central.

### Que faire si *git pull* ne fonctionne pas ?

> git add .
> 
> git commit -m "Mon message"

puis

> git pull

### Comment faire pour importer le projet pour la premiere fois ?

Va dans le dossier ou tu veut ranger le projet. Click droit -> ouvrir un terminal (Tilix je crois). Tape les commandes suivantes :

> git config --global user.name "Norman Kheid"

> git config --global user.email tonAdresseMail@gmailcom

> git clone https://gitlab.com/DrTruite/boulvaietgornite.git

Rentre tes identifiants et mot de passe.