extends KinematicBody2D

const GRAVITY = 20
const SPEED = 100
const FLOOR = Vector2(0, -1)

var velocity = Vector2()
var direction = 1
var walking  = true

func _ready():
	velocity.x = 0
	velocity.y = 0

func _physics_process(delta):
	if walking:
		velocity.x = SPEED * direction
	
	if direction == 1:
		$Sprite.flip_h = false
	else:
		$Sprite.flip_h = true
	
	velocity.y += GRAVITY
	
	velocity = move_and_slide(velocity, FLOOR, true)
	
	if $RayCast2D.is_colliding() == false:
		velocity.x = 0
		walking = false
		if $Turn_Around_Timer.is_stopped():
			$Turn_Around_Timer.start()

func _on_Turn_Around_Timer_timeout():
	direction = direction * -1
	$RayCast2D.position.x *= -1
	walking = true


func _on_DamageArea_body_entered(body):
	if body.is_in_group("Player"):
		var push_vector = Vector2(- self.position.x + body.position.x, - self.position.y + body.position.y).normalized()
		body.get_damage(1.0, push_vector)
