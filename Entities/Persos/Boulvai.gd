extends KinematicBody2D

const SPEED = 300
const ACCELERATION = 60
const DECELERATION = 30
const GRAVITY = 20
const JUMP_POWER = -600
const FLOOR = Vector2(0, -1)

var direction = 1
var velocity = Vector2()
var on_ground = false
var is_attacking = false

var health = 5
var is_invicible = 0.0

func _ready():
	$Interface/UI/HP_Container/HP_Bar.value = health

func _physics_process(delta):
	if Input.is_action_pressed("ui_right"):
		direction = 1
		if velocity.x < SPEED:
			velocity.x += min(ACCELERATION, SPEED - velocity.x)
	elif Input.is_action_pressed("ui_left"):
		direction = -1
		if velocity.x > -200:
			velocity.x -= max(ACCELERATION, - SPEED + velocity.x)
	else:
		if velocity.x > 0:
			velocity.x -= min(DECELERATION, velocity.x)
		elif velocity.x < 0:
			velocity.x += max(DECELERATION, velocity.x)
	
	if Input.is_action_pressed("platform_jump"):
		if on_ground == true:
			velocity.y = JUMP_POWER
		
	velocity.y += GRAVITY
	
	if is_on_floor():
		on_ground = true
		$AnimationPlayer.play("Rest_Pose")
	else:
		on_ground = false
		$AnimationPlayer.play("Jump")
	
	velocity = move_and_slide(velocity, FLOOR, true)
	
	$Node2D.scale.x = direction
	
	is_invicible = max(is_invicible - delta, 0)
	if is_invicible > 0:
		# Halve opacity every uneven frame counts
		self.modulate.a = 0.0 if Engine.get_frames_drawn() % 2 == 0 else 1.0
	else:
		# But beware... if the last damage frame is not even,
		# you risk to leave your character half transparent!
		# Preferably do this when you set your flag back to false
		self.modulate.a = 1.0

func get_damage(amount, push_vector):
	# Push
	velocity += (push_vector * 700)
	
	# Screen effects
	$Camera2D.add_trauma(amount)
	
	# Damage
	if is_invicible <= 0:
		is_invicible += 0.5
		health -= amount
		$Interface/UI/HP_Container/HP_Bar.value -= amount
		if health <= 0:
			get_tree().change_scene("res://levels/game_over.tscn")
