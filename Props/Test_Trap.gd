extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Node2D_body_entered(body):
	if body.is_in_group("Player"):
		var push_vector = Vector2(- self.position.x + body.position.x, - self.position.y + body.position.y).normalized()
		body.get_damage(1.0, push_vector)
