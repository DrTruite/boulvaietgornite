## 4 Chevaliers de l'apocalypse

### Conquete
- Couleur: <span style="color:white; text-shadow: 1px 1px black; text-shadow: -1px 1px black;">■</span>Blanc
- Symbolisme: Puissance, victoire
- Attribut: Arc
- colorswatch: 

### Guerre
- Couleur: <span style="color:red;">■</span>Rouge
- Symbolisme: Sang, Violence
- Attribut: Épée

### Famine
- Couleur: <span style="color:black;">■</span>Noir
- Symbolisme: Manque
- Attribut: Balance

### Epidémie
- Couleur: <span style="color:green;">■</span>Vert
- Symbolisme: Peur
- Attribut: Mort
